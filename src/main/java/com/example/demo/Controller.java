package com.example.demo;

import org.springframework.boot.Banner;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import java.util.ArrayList;
import java.util.List;

@RestController
public class Controller {
    List<String> list = new ArrayList<>();

    @ResponseBody
    @CrossOrigin
    @GetMapping("/main")
    public ModelAndView registers(Model model) {
        model.addAttribute("names", list);
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("main");
        return modelAndView;
    }

    @GetMapping("/a")
    public String add(Model model) {
        list.add("Elgun");
        return "OK";
    }

}
