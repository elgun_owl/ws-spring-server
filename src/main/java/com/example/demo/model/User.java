package com.example.demo.model;

import javax.websocket.Session;

public class User {

    private  Session session = null;
    private  String name;
    private  String data = "none";

    public User() {
    }

    public  String getData() {
        return data;
    }

    public  void setData(String data) {
        this.data = data;
    }

    public void setSession(Session session) {
        this.session = session;
    }

    public  Session getSession() {
        return session;
    }

    public   String getName() {
        return name;
    }

    public  void setName(String name) {
        this.name = name;
    }
}
