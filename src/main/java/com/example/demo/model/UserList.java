package com.example.demo.model;

import java.util.HashMap;

public class UserList {
    public static HashMap<String, User> clientList = new HashMap<>();

    public  UserList() {

    }

    public static HashMap<String, User> getClientList() {
        return clientList;
    }

    public static void addUserToList(String userID, User user) {
        clientList.put(userID, user);
    }

    public static User getUserByID(String userID) {
        return clientList.get(userID);
    }

    public static void setClientList(HashMap<String, User> clientList) {
        UserList.clientList = clientList;
    }
}
