package com.example.demo.weboscket;

import com.example.demo.model.AdminCommands;
import com.example.demo.model.User;
import com.example.demo.model.UserList;
import com.google.gson.Gson;
import org.springframework.stereotype.Component;

import javax.websocket.OnClose;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;
import java.io.IOException;

@Component
@ServerEndpoint(value = "/wss")
public class EndPointAdmin {

    public User user;
    private User admin;

    @OnOpen
    public void onOpen(Session session) throws IOException {
        System.out.println("Client Admin connect");
        admin = new User();
        admin.setSession(session);
        admin.setName("admin");
        UserList.addUserToList("admin", admin);
    }


    @OnMessage
    public void onText(Session session, String dataFromAdmin) throws IOException {

        Gson g = new Gson();
        AdminCommands p = g.fromJson(dataFromAdmin, AdminCommands.class);
        user = UserList.getUserByID(p.getId());

        if(user!= null){
            if(user.getSession() != null && user.getSession().isOpen()) {
                user.getSession().getBasicRemote().sendText(p.getCommand());
            }
        } else {
            session.getBasicRemote().sendText("User is disconnected");
        }

    }

    @OnClose
    public void onClose(Session session) {
        System.out.println("Client disconnect" + session.getId());
    }

}
