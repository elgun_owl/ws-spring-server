package com.example.demo.weboscket;

import com.example.demo.model.User;
import com.example.demo.model.UserList;
import org.springframework.stereotype.Component;

import javax.websocket.OnClose;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.PathParam;
import javax.websocket.server.ServerEndpoint;
import java.io.IOException;
import java.util.HashMap;

@Component
@ServerEndpoint(value = "/ws/{username}")
public class EndPoint {

    public User user;
    

    @OnOpen
    public void onOpen(Session session, @PathParam("username") String username) throws IOException {
        user = new User();
        user.setSession(session);
        user.setName(username);
        UserList.addUserToList(username,user);
        System.out.println(username + " is connected");
    }




    @OnMessage
    public void onText(String message) throws IOException {
        User userAdmin = UserList.getUserByID("admin");
        userAdmin.getSession().getBasicRemote().sendText(message);

    }

    @OnClose
    public void onClose(Session session) {
        System.out.println("Client disconnect" + session.getId());
    }
}
